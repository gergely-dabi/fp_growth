Frequency pattern growth algorithm for mining frequent itemsets in a given
dataset as described in

Data mining : concepts and techniques / Jiawei Han, Micheline Kamber, Jian Pei.
– 3rd ed. p. cm. ISBN 978-0-12-381479-1

The same repo contains an implementation of the apriori algorithm to test
against the the ferquency pattern growth algorithm, and to compare runtime on
randomly generated itemsets